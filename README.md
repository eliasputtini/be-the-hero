![enter image description here](./frontend/src/assets/logo.svg)

# Semana OmniStack 11.0
#### [RocketSeat.com.br](https://rocketseat.com.br/)


O Be The Hero é o projeto da Rocket Seat, que tem a função de auxiliar a comunicação de ONG's com seus doadores.


## Prerequisitos


Utilize algum instalador de pacotes como yarn ou npm para escrever os comandos a seguir no cmd


## Instalação



### Backend 

Abra a pasta `./backend/`  no cmd e digite: 

```
npm install
```
```
npm start

```

### Frontend 


Abra a pasta `./frontend/` no cmd e digite: 


```
yarn install
```
```
yarn start
```
### Mobile 

Abra a pasta `./mobile/` no cmd e digite: 


```
yarn install
```
```
yarn start
```

#### Feito a partir de:
- Node.js
- React
- React Native
- SQLite

